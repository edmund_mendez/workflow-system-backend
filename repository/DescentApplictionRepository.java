package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.DescentApplication;

public interface DescentApplictionRepository extends JpaRepository<DescentApplication, Long> {

}

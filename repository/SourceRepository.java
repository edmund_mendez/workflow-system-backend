package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.ApplicationSource;

public interface SourceRepository extends JpaRepository<ApplicationSource, Integer> {
	public ApplicationSource findBySourceName(String name);
}

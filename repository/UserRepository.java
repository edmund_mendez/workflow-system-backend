package jm.gov.pica.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import jm.gov.pica.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	public User findByUsername(String username);
	
	@Query("SELECT u.username, u.firstName, u.lastName, COUNT(a) FROM Application a RIGHT JOIN a.currentOwner u "
			+ "WHERE u IN "
			+ "(Select u FROM User u INNER JOIN u.roles r WHERE r.name='ROLE_ADMIN') "
			+ "GROUP BY u.username")
	Collection<Object[]> findUserWorkload();
/****
 * NATIVE:
select users.username, count(application.owner_id) from 
users left outer join application on application.owner_id = users.id 
where application.owner_id in 
(select users.id from users inner join users_roles on users.id=users_roles.user_id inner join roles on users_roles.role_id=roles.id where roles.name="ROLE_ADMIN") 
group by application.owner_id;
 * 
 */
	
}

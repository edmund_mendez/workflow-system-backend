package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationAttachment;

public interface AttachmentRepository extends JpaRepository<ApplicationAttachment, Integer> {
	public ApplicationAttachment findOneByApplicationAndFileType(Application application, String filetype);
}

package jm.gov.pica.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

public interface ApplicationOnlyProjection {
	long getId();
	String getFirstName();
	String getMiddleName();
	String getLastName();
	String getApplicationType();
	String getState();
	Date getCreationDate();
	/*
	 @Value("#{target.currentOwner.getUsername()}")
	String getOwner();*/
}

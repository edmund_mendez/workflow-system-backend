package jm.gov.pica.repository;

import org.springframework.data.repository.CrudRepository;

import jm.gov.pica.model.Privilege;

public interface PrivilegeRepository extends CrudRepository<Privilege, Long> {
	public Privilege findByName(String name);
}

package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {
	public Country findByName(String name);
}

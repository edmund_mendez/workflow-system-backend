package jm.gov.pica.repository;

import org.springframework.data.repository.CrudRepository;

import jm.gov.pica.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	public Role findByName(String name);
}

package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationActivity;

public interface ActivityRepository extends JpaRepository<ApplicationActivity, Long> {
	public ApplicationActivity findOneByApplicationAndIsCompleted(Application application, Boolean isCompleted);
}

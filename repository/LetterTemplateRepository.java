package jm.gov.pica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jm.gov.pica.model.LetterTemplate;

public interface LetterTemplateRepository extends JpaRepository<LetterTemplate, Long> {

}

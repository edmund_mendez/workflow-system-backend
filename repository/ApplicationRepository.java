package jm.gov.pica.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.User;
//import jm.gov.pica.model.StateActivity;

public interface ApplicationRepository extends JpaRepository<Application, Long> {
	Collection<ApplicationOnlyProjection> findAllByOrderByIdDesc();
	
	Collection<ApplicationOnlyProjection> findAllByCurrentOwnerOrderByIdDesc(User owner);
	
	Collection<ApplicationOnlyProjection> findAllByStateOrderByIdDesc(String state);
	
	/*@Query("SELECT act.application.firstName,act.application.middleName,act.application.lastName,"
			+ "act.application.applicationType,act.application.state "
			+ "FROM StateActivity act INNER JOIN act.application app "
			+ "WHERE act.isCompleted='false' AND app.currentOwner=:user")
	Collection<Object> findAllByCurrentOwnerActivity(@Param("user") User owner);
	*/
	
	//@Query("SELECT f FROM Foo f WHERE LOWER(f.name) = LOWER(:name)")
	//Collection<Application> getAllSorted(@Param("name") String name);
	
	
	/*
	 List<Object[]> results = entityManager
        .createQuery("SELECT m.name AS name, COUNT(m) AS total FROM Man AS m GROUP BY m.name ORDER BY m.name ASC");
        .getResultList();
	 */
}

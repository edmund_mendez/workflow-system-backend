package jm.gov.pica.services;

import java.util.Collection;

import jm.gov.pica.model.ApplicationSource;

public interface SourceService {
	public ApplicationSource create(ApplicationSource ApplicationSource);
	public ApplicationSource findOne(Long id);
	public ApplicationSource update(ApplicationSource ApplicationSource);
	public boolean delete(long id);
	public Collection<ApplicationSource> findAll();
}

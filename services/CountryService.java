package jm.gov.pica.services;

import java.util.Collection;

import jm.gov.pica.model.Country;

public interface CountryService {
	public Country create(Country Country);
	public Country findOne(Long id);
	public Country update(Country Country);
	public boolean delete(long id);
	public Collection<Country> findAll();
}

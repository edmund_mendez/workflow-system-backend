package jm.gov.pica.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.opensagres.xdocreport.template.velocity.internal.VelocityTemplateEngine;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.images.FileImageProvider;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import jm.gov.pica.dto.CertDataDescent;
import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationAttachment;
import jm.gov.pica.model.DescentApplication;
import jm.gov.pica.model.LetterTemplate;
import jm.gov.pica.repository.AttachmentRepository;

@Service
public class PublisherServiceImpl implements PublisherService {
	@Autowired
	AttachmentService attService;
	@Autowired
	AttachmentRepository attRepository;
	
	@Override
	public void genCertificate(Application application, LetterTemplate template, OutputStream out) {
		File tmplFile = attService.getTemplateFile(template);
		FileInputStream in = null;
		
		try {
			in = new FileInputStream(tmplFile);
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport( in, TemplateEngineKind.Velocity);
			IContext context = report.createContext();
			this.setData(report, context, application);
			report.process(context, out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XDocReportException e) {
			e.printStackTrace();
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void setData(IXDocReport report, IContext context, Application application){
		
         
		try {
			ApplicationAttachment photo = attRepository.findOneByApplicationAndFileType(application,"1");
			FieldsMetadata metadata = new FieldsMetadata();
			metadata.addFieldAsImage("photo");
			report.setFieldsMetadata(metadata);
	         //context.put("photo", new FileImageProvider(attService.getAppFile(photo),true));
			context.put("photo", new FileImageProvider(attService.getAppFile(photo)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		if(application.getApplicationType().equals("DESCENT")){
			CertDataDescent ctxData = new CertDataDescent();
			ctxData.transferData((DescentApplication)application);
			context.put("application", ctxData);
		}else if(application.getApplicationType().equals("RENUNCIATION")){}
	}
}

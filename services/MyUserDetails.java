package jm.gov.pica.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import jm.gov.pica.model.Privilege;
import jm.gov.pica.model.Role;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.UserRepository;

@Component
@Transactional
public class MyUserDetails implements UserDetailsService {
	
	@Autowired
	UserRepository userDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		//User user = userDao.findByUsername(username);
		User user = userDao.findByUsername(username.toLowerCase(Locale.ENGLISH));
		if(user==null){
			throw new UsernameNotFoundException("User not found");
		}
		return new org.springframework.security.core.userdetails.User(
		          user.getUsername(), user.getPassword(), user.isEnabled(), true, true, 
		          true, getAuthorities(user.getRoles()));
	}
	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }
	
	private List<String> getPrivileges(Collection<Role> roles) {
	        List<String> privileges = new ArrayList<String>();
	        List<Privilege> collection = new ArrayList<Privilege>();
	        for (Role role : roles) {
	            collection.addAll(role.getPrivileges());
	            privileges.add(role.getName());
	        }
	        for (Privilege item : collection) {
	            privileges.add(item.getName());
	        }
	        
	        return privileges;
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}

package jm.gov.pica.services;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationActivity;
import jm.gov.pica.model.User;

public interface StateManagerService {
	public boolean completeActivity(Application application, String event);
	public Application completeActivity(ApplicationActivity activity, String event, User user);
	public ApplicationActivity startActivity(Application application, User user);
	//public String getFirstState();
}

package jm.gov.pica.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jm.gov.pica.dto.NewApplication;
import jm.gov.pica.model.Application;
import jm.gov.pica.model.DescentApplication;
import jm.gov.pica.model.RenunciationApplication;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.ApplicationOnlyProjection;
import jm.gov.pica.repository.ApplicationRepository;
import jm.gov.pica.repository.UserRepository;

@Component
public class ApplicationServiceImpl implements ApplicationService {
	//@PersistenceContext EntityManager entityManager; 
	
	@Autowired
	private ApplicationRepository appRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StateManagerService activityService;
	
	@Override
	public Application createFromDTO(NewApplication newAppDTO, User user) {
		Application app=null;
		if(newAppDTO.getApplicationType().equals("DESCENT")){
			app = new DescentApplication(null, newAppDTO.getFirstName(), newAppDTO.getMiddleName(), newAppDTO.getLastName(), newAppDTO.getGender(), newAppDTO.getBirthDate(), null, null, "", null,null,null);
		}else if(newAppDTO.getApplicationType().equals("RENUNCIATION")){
			app = new RenunciationApplication(null, newAppDTO.getFirstName(), newAppDTO.getMiddleName(), newAppDTO.getLastName(), newAppDTO.getGender(), newAppDTO.getBirthDate(), null, null, "", null,null,null);
		}
		app.setApplicationType(newAppDTO.getApplicationType());
		app.setCurrentOwner(user);
		activityService.startActivity(app,user);
		return appRepository.save(app);
	}

	@Override
	public Application findOne(Long id) {
		return appRepository.findOne(id);
	}

	@Override
	public Application update(Application application) {
		Application oldApp = appRepository.findOne(application.getId()); 
		if(oldApp==null){
			return null;
		}
		application.setActivities(oldApp.getActivities()); //to fix problem with losing children after save
		return appRepository.save(application);
	}

	@Override
	public boolean delete(long id) {
		try{
			appRepository.delete(id);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Collection<Application> findAll() {
		return appRepository.findAll();
	}

	@Override
	public Collection<ApplicationOnlyProjection> findAllSorted() {
		// TODO Auto-generated method stub
		return appRepository.findAllByOrderByIdDesc();
	}

	@Override
	public Collection<ApplicationOnlyProjection> findAllByOwner(User owner) {
	
		return appRepository.findAllByCurrentOwnerOrderByIdDesc(owner);
		//return appRepository.findAllByCurrentOwnerActivity(owner);
		/*TypedQuery<Application> qry = entityManager.createQuery("SELECT act.application.firstName, act.application.middleName, act.application.lastName "
				+ "FROM StateActivity act INNER JOIN act.application app "
				+ "WHERE act.isCompleted='false' AND app.currentOwner=:user",Application.class);
		qry.setParameter("user", owner);
		Collection<Application> res = qry.getResultList();
		entityManager.close();
		return res;*/
	}

	@Override
	public Application create(Application application, User user) {
		activityService.startActivity(application,user);
		return appRepository.save(application);
		
	}

	@Override
	public Collection<ApplicationOnlyProjection> findRestricted(String restriction, User owner) {
		Collection<ApplicationOnlyProjection> applications = null;
		switch(restriction){
		case "owned":
			applications =  appRepository.findAllByCurrentOwnerOrderByIdDesc(owner);
			break;
		case "unclaimed":
			applications =  appRepository.findAllByCurrentOwnerOrderByIdDesc(null);
			break;
		case "completed":
			applications =  appRepository.findAllByStateOrderByIdDesc("Completed");
			break;
		}
		
		return applications;
	}

	@Override
	public Application claimApplication(Application application, User owner) {
		String roles = application.getClaimRoles();
		String[] claimableRoles={};
		if(roles!=null){
			claimableRoles=roles.split(",");
		}
		//System.out.println("roles string is "+roles+" and after split array size is "+claimableRoles.length);
		//check if user has any of the required roles to claim
		if(owner.hasRole(claimableRoles)){
			application.setCurrentOwner(owner);
			return appRepository.save(application);
		}
		return application;
	}

	@Override
	public Application assignCaseOfficerByUsername(Application application, String username) {
		User officer = userRepository.findByUsername(username);
		if(officer!=null){
			application.setCaseOfficer(officer);
			appRepository.save(application);
		}else{
			//throw new UserNotFoundException("user with username "+username+" not in database");
		}
			
		return application;
	}

	@Override
	public Application assignCaseOfficerByUserId(Application application, String username) {
		// TODO Auto-generated method stub
		return null;
	}

}

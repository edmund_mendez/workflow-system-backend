package jm.gov.pica.services;

import java.util.Collection;

import jm.gov.pica.dto.UserLoadSummary;

public interface UserService {
	public Collection<UserLoadSummary> getUserLoadSummary(String role);
}

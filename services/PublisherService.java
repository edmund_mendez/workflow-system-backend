package jm.gov.pica.services;

import java.io.OutputStream;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.LetterTemplate;

public interface PublisherService {
	public void genCertificate(Application application, LetterTemplate template,OutputStream out);
}

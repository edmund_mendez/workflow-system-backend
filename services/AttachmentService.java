package jm.gov.pica.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;

import org.springframework.web.multipart.MultipartFile;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationAttachment;
import jm.gov.pica.model.ApplicationType;
import jm.gov.pica.model.LetterTemplate;


public interface AttachmentService {
	public ApplicationAttachment createAppAttachment(Application application, MultipartFile file, String fileType);
	public Collection<ApplicationAttachment> getAppAttachments(Application application);
	public ApplicationAttachment getAppAttachmentFile(int id);
	public File getAppFile(ApplicationAttachment attachment) throws FileNotFoundException;
	public File viewAppImageFile(Long id, String filename);
	
	public LetterTemplate createTemplate(MultipartFile file,String templateName, ApplicationType applicationType);
	public void createMockTemplate(File file, String templateName, ApplicationType applicationType);
	public File getTemplateFile(LetterTemplate template);
	public LetterTemplate getTemplate(Long id);
}

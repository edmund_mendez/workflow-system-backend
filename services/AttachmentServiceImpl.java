package jm.gov.pica.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationAttachment;
import jm.gov.pica.model.ApplicationType;
import jm.gov.pica.model.LetterTemplate;
import jm.gov.pica.repository.AttachmentRepository;
import jm.gov.pica.repository.LetterTemplateRepository;
@Service
public class AttachmentServiceImpl implements AttachmentService {
	@Autowired
	private Environment env;
	@Autowired
	AttachmentRepository attRepository;
	
	@Autowired
	LetterTemplateRepository templateRepository;
	
	@Override
	public ApplicationAttachment createAppAttachment(Application application, MultipartFile file, String fileType) {
		String subFolder="app-"+application.getId();
		ApplicationAttachment attachment=null;
		String filename = file.getOriginalFilename();
		
		/*if(fileType.equals("1")){
			int dotpos = filename.lastIndexOf('.');
			String fext = dotpos > 0 ? filename.substring(dotpos) : "";
			filename="photo"+fext;
		}*/
		
        if (writeFile(file,env.getProperty("app-upload-path"),subFolder,filename)) {
        	//once file is saved, now create reference to it
        	attachment = new ApplicationAttachment();
        	attachment.setFileType(fileType);
        	attachment.setLocalPath(env.getProperty("app-upload-path")+subFolder+"/");
        	//attachment.setName(file.getOriginalFilename());
        	attachment.setName(filename);
        	attachment.setSize((int)file.getSize());
        	attachment.setUploadDate(new Date());
        	attachment.setApplication(application);
        }
		return attachment;
	}
	
	private boolean writeFile(MultipartFile file, String path, String subFolder, String filename){
		/*byte[] bytes;
		FileOutputStream fos=null;*/
		if (!file.isEmpty()) {
			
			try {
				  File sFolder = new File(path+subFolder+"/");
		             if(!sFolder.exists()){
		            	 sFolder.mkdirs();
		             }
				Files.copy(file.getInputStream(), Paths.get(path+subFolder+"/"+filename));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	/*try{
             bytes = file.getBytes();
             File sFolder = new File(path+subFolder+"/");
             if(!sFolder.exists()){
            	 sFolder.mkdirs();
             }
             fos = new FileOutputStream(path+subFolder+"/"+filename);
             fos.write(bytes);
             fos.close();
        	}catch(IOException e){
        		e.printStackTrace();
        	}finally{
        		try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}*/
		}else{
			return false;
		}
		return true;
	}
	
	@Override
	public Collection<ApplicationAttachment> getAppAttachments(Application application) {
		return null;
	}

	@Override
	public ApplicationAttachment getAppAttachmentFile(int id) {
		return attRepository.findOne(id);
	}

	@Override
	public File getAppFile(ApplicationAttachment attachment) throws FileNotFoundException {
		File file = new File(attachment.getLocalPath()+attachment.getName());
		if(!file.exists()){
			throw new FileNotFoundException("File not found");
		}
		return file;
	}

	@Override
	public File viewAppImageFile(Long id, String filename) {
		String subFolder="App-"+id;
		return new File(env.getProperty("app-upload-path")+subFolder+"/"+filename);
	}

	@Override
	public LetterTemplate createTemplate(MultipartFile file, String templateName, ApplicationType applicationType) {
		LetterTemplate template=null;
		String subFolder = "templates/"+applicationType.toString();
		String filename=file.getOriginalFilename();
		if (writeFile(file,env.getProperty("app-upload-path"),subFolder,filename)){
		 template = new LetterTemplate();
		template.setApplicationType(applicationType);
		template.setFileName(file.getOriginalFilename());
		template.setTemplateName(templateName);
		templateRepository.save(template);
		}
		return template;
	}
	@Override
	public void createMockTemplate(File file, String templateName, ApplicationType applicationType){
		String subFolder = "templates/"+applicationType.toString();
		LetterTemplate template = new LetterTemplate();
		String targetFolder = env.getProperty("app-upload-path")+"/"+subFolder;
		
		File tFolder = new File(targetFolder);
		if(!tFolder.exists()){
			tFolder.mkdirs();
		}
			
		File target = new File(targetFolder+"/"+file.getName());
		
		try {
			Files.copy(file.toPath(), target.toPath(),StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		template.setApplicationType(applicationType);
		template.setFileName(file.getName());
		template.setTemplateName(templateName);
		templateRepository.save(template);
	}

	@Override
	public File getTemplateFile(LetterTemplate template) {
		String subFolder = "templates/"+template.getApplicationType().toString();
		String targetFolder = env.getProperty("app-upload-path")+"/"+subFolder;
		File file= new File(targetFolder+"/"+template.getFileName());
		return file;
	}

	@Override
	public LetterTemplate getTemplate(Long id) {
		return templateRepository.findOne(id);
	}

}

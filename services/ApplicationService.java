package jm.gov.pica.services;

import java.util.Collection;

import jm.gov.pica.dto.NewApplication;
import jm.gov.pica.dto.UserLoadSummary;
import jm.gov.pica.model.Application;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.ApplicationOnlyProjection;

public interface ApplicationService {
	public Application createFromDTO(NewApplication newApplication, User user);
	public Application create(Application application, User user);
	public Application findOne(Long id);
	public Application update(Application Application);
	public Application claimApplication(Application Application, User user);
	public boolean delete(long id);
	public Collection<Application> findAll();
	public Collection<ApplicationOnlyProjection> findAllSorted();
	public Collection<ApplicationOnlyProjection> findAllByOwner(User user);
	public Collection<ApplicationOnlyProjection> findRestricted(String restriction, User user);
	public Application assignCaseOfficerByUsername(Application application, String username);
	public Application assignCaseOfficerByUserId(Application application, String username);
	
}

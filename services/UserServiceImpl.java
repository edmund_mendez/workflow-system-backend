package jm.gov.pica.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jm.gov.pica.dto.UserLoadSummary;
import jm.gov.pica.repository.UserRepository;

@Component
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;
	
	@Override
	public Collection<UserLoadSummary> getUserLoadSummary(String role) {
		ArrayList<UserLoadSummary> loadSummary= new ArrayList<UserLoadSummary>();
		Collection<Object[]> res = userRepository.findUserWorkload();
		for(Object[] r : res){
			loadSummary.add(new UserLoadSummary(r[0].toString(),r[1].toString(),r[2].toString(),Integer.parseInt(r[3].toString())));
		}
		return loadSummary;
	}

}

package jm.gov.pica.services;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationActivity;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.ActivityRepository;
import jm.gov.pica.repository.ApplicationRepository;
import jm.gov.pica.repository.UserRepository;
import jm.gov.pica.statemachine.Persist;
import jm.gov.pica.statemachine.StateMachineActivatable;
import jm.gov.pica.statemachine.StateMachineConfig;

@Component
public class StateManagerServiceImpl implements StateManagerService {
	@Autowired
	ApplicationService appService;
	
	@Autowired
	Persist persist;
	
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private ApplicationRepository appRepository;
	
	@Autowired
	StateMachineConfig sMConfigService;
	
	@Override
	public boolean completeActivity(Application application, String event) {
		
		/*for( ApplicationActivity oldActivity : application.getActivities()){
			if(!oldActivity.isCompleted()){
				oldActivity.setCompleted(true);
				oldActivity.setCompletedBy(userRepository.findByUsername(SecurityUtils.getCurrentUserLogin()));
				oldActivity.setDateCompleted(new Date());
				break;
			}
		}
		
		persist.change(application, event);
		//System.out.println("after persist.change, state is: "+application.getState());
		ApplicationActivity activity = new ApplicationActivity();
		activity.setSmState(application.getState());
		activity.setApplication(application);
		activity.setCompleted(false);
		activity.setDateStarted(new Date());
		activity.setArchived(false);
		application.getActivities().add(activity);
		appService.update(application);*/
		return true;
	}

	@Override
	public ApplicationActivity startActivity(Application application, User currentUser) {
		persist.change(application,"*START*");
		
		StateMachineActivatable newActivity = new ApplicationActivity();
		HashMap<String,User> ownerFields = new HashMap<String,User>();
		ownerFields.put("caseOfficer", application.getCurrentOwner());	//TEMPORARY MEASURE...NO CASE OFFICER
		
		sMConfigService.ConfigureAcitivity(application.getState(),newActivity, application.getActivities(), currentUser, ownerFields);
		
		ApplicationActivity activity = (ApplicationActivity)newActivity;

		activity.setSmState(application.getState());
		activity.setApplication(application);
		activity.setCompleted(false);
		activity.setDateStarted(new Date());
		activity.setArchived(false);
		application.getActivities().add(activity);
		return activity;
	}

	@Override
	public Application completeActivity(ApplicationActivity activityDTO, String event, User currentUser) {
		ApplicationActivity activity = activityRepository.findOne(activityDTO.getId());	//need this lookup so we can get a managed version of activity (and by extension application)
		Application application=null;
		
		if(activity!=null){
			application = activity.getApplication();
			for( ApplicationActivity oldActivity : application.getActivities()){
				if(oldActivity.getId() == activityDTO.getId()){
					oldActivity.setCompleted(true);
					oldActivity.setCompletedBy(currentUser);
					oldActivity.setDateCompleted(new Date());
					oldActivity.setComments(activityDTO.getComments());
					if(oldActivity.isRequireApproval()){
						oldActivity.setApprovalResult(activityDTO.getApprovalResult());
					}
					break;
				}
			}
			
		}
		
		persist.change(application, event);
		
		
		HashMap<String,User> ownerFields = new HashMap<String,User>();
		ownerFields.put("caseOfficer", application.getCaseOfficer());	//TEMPORARY MEASURE...NO CASE OFFICER
		
		StateMachineActivatable newActivityIf = new ApplicationActivity();
		sMConfigService.ConfigureAcitivity(application.getState(),newActivityIf, application.getActivities(), currentUser, ownerFields);
		
		
		ApplicationActivity newActivity = (ApplicationActivity)newActivityIf;
		if(newActivity.getAutomaticOwner()!=null){
			application.setCurrentOwner(newActivity.getAutomaticOwner());
			application.setClaimRoles("");
		}else{
			application.setClaimRoles(newActivity.getClaimGroups());
			application.setCurrentOwner(null);
		}
		//ApplicationActivity newActivity = new ApplicationActivity();
		newActivity.setSmState(application.getState());
		newActivity.setApplication(application);
		newActivity.setCompleted(false);
		newActivity.setDateStarted(new Date());
		newActivity.setArchived(false);
		application.getActivities().add(newActivity);
		return appRepository.save(application);
	}
}

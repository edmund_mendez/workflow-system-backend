package jm.gov.pica.statemachine;

import jm.gov.pica.model.User;
/*
 *Interface to control using client provided Model to handle state configuration
 *in this case, using the Entity model Activity
 * */
public interface StateMachineActivatable {
	public void setSMInstructions(String instructions);
	public void setSMAutomaticOwner(User owner);
	public void setSMRolesThatCanClaim(String roles);
	public void setSMIsDecisionNode(boolean isDecision);
	public String getSMActivityState();
	public User getSMUserThatCompleted();
}

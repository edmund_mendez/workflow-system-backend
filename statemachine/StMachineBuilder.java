package jm.gov.pica.statemachine;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.StateMachineBuilder.Builder;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

@Component
@EnableStateMachine(name = "stateMachineTarget")
public class StMachineBuilder {
	@Bean(name = "stateMachineTarget")
	@Scope(scopeName="prototype")
	StateMachine<String, String> buildMachine1() throws Exception {
		Builder<String, String> builder = StateMachineBuilder.<String,String>builder();

		builder.configureConfiguration()
		.withConfiguration()
			.autoStartup(true);
		
		builder.configureStates()
		.withStates()
		.initial("ENTER_APP_DETAILS")
		.state("ATTACH_SUPP_DOCS")
		.state("ASSIGN_OFFICER")
		.state("FIRST_REVIEW")
		.state("PREPARE_CERT")
		.state("SECOND_REVIEW")
		.state("DIRECTOR_ADJ")
		.state("PREP_DECL_LETTER")
		.state("PREP_APPR_LETTER")
		.state("ATTACH_LETTER")
		.state("DISPATCH_LETTER")
		.state("COMPLETE_APPLICATION");

		builder.configureTransitions()
			.withExternal()
				.source("ENTER_APP_DETAILS")
				.target("ATTACH_SUPP_DOCS")
				.event("COMPLETE").and()
			.withExternal()
				.source("ATTACH_SUPP_DOCS")
				.target("ASSIGN_OFFICER")
				.event("COMPLETE").and()
			.withExternal()
				.source("ASSIGN_OFFICER")
				.target("FIRST_REVIEW")
				.event("COMPLETE").and()
			.withExternal()
				.source("FIRST_REVIEW").target("PREPARE_CERT")
				.event("APPROVE").and()
			.withExternal()
				.source("FIRST_REVIEW").target("SECOND_REVIEW")
				.event("DECLINE").and()
			.withExternal()
				.source("PREPARE_CERT")
				.target("SECOND_REVIEW")
				.event("COMPLETE").and()
			.withExternal()
				.source("SECOND_REVIEW").target("FIRST_REVIEW")
				.event("DECLINE").and()
			.withExternal()
				.source("SECOND_REVIEW").target("DIRECTOR_ADJ")
				.event("APPROVE").and()
			.withExternal()
				.source("DIRECTOR_ADJ").target("PREP_DECL_LETTER")
				.event("DECLINE").and()
			.withExternal()
				.source("DIRECTOR_ADJ").target("PREP_APPR_LETTER")
				.event("APPROVE").and()
			.withExternal()
				.source("PREP_DECL_LETTER")
				.target("ATTACH_LETTER")
				.event("COMPLETE").and()
			.withExternal()
				.source("PREP_APPR_LETTER")
				.target("ATTACH_LETTER")
				.event("COMPLETE").and()
			.withExternal()
				.source("ATTACH_LETTER")
				.target("DISPATCH_LETTER")
				.event("COMPLETE").and()
			.withExternal()
				.source("DISPATCH_LETTER")
				.target("COMPLETE_APPLICATION")
				.event("COMPLETE");
		return builder.build();
	}

	@Bean
	public StateMachineConfig statesConfig(){
		return StateMachineConfig
				.startConfig()
					.forState("ENTER_APP_DETAILS")
						.instructions("Fill out all fields")
						.officerAssignCurrent().and()
					.forState("ATTACH_SUPP_DOCS")
						.instructions("Attach all supporting documents provided by applicant")
						.officerAssignCurrent().and()
					.forState("ASSIGN_OFFICER")
						.instructions("Select Case Officer to assign")
						.officerClaimsWithRole("ROLE_PICA_MANAGER").and()
					.forState("FIRST_REVIEW")
						.instructions("review and approve")
						.requireApproval()
						.officerAssignFromField("caseOfficer").and()
					.forState("PREPARE_CERT")
						.instructions("")
						.officerAssignFromField("caseOfficer").and()
					.forState("SECOND_REVIEW")
						.instructions("Review and approve if application qualifies")
						.officerClaimsWithRole("ROLE_PICA_MANAGER")	//can also use previous acitivty-based assignment
						.requireApproval().and()
					.forState("DIRECTOR_ADJ")
						.instructions("")
						.officerClaimsWithRole("ROLE_PICA_DIRECTOR")
						.requireApproval().and()
					.forState("PREP_DECL_LETTER")
						.instructions("")
						.officerAssignFromField("caseOfficer").and()
					.forState("PREP_APPR_LETTER")
						.instructions("")
						.officerAssignFromField("caseOfficer").and()
					.forState("ATTACH_LETTER")
						.instructions("")
						.officerClaimsWithRole("ROLE_PICA_DESK_OFFICER").and()
					.forState("DISPATCH_LETTER")
						.instructions("")
						.officerClaimsWithRole("ROLE_PICA_DESK_OFFICER").and()
					.forState("COMPLETE_APPLICATION")
						.instructions("")
						.officerClaimsWithRole("ROLE_PICA_DESK_OFFICER")
						.and()
					.Build();
	}
}

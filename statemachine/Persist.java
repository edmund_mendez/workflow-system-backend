package jm.gov.pica.statemachine;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler.PersistStateChangeListener;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import jm.gov.pica.model.Application;

@Component
public class Persist {
	private final PersistStateMachineHandler handler;
	//private final LocalPersistStateChangeListener listener = new LocalPersistStateChangeListener();
	private StateMachine<String,String> stateMachine;
	
	public Persist(PersistStateMachineHandler handler, StateMachine<String, String> sm) {
		this.handler = handler;
		this.handler.addPersistStateChangeListener(new LocalPersistStateChangeListener());
		this.stateMachine=sm;
		
	}

	public void change(Application application, String event) {
		if(event.equals("*START*")){
			application.setState(this.stateMachine.getInitialState().getId());
		}else{
			handler.handleEventWithState(MessageBuilder.withPayload(event).setHeader("application", application).build(), application.getState());
		}
		
	}
	
	private class LocalPersistStateChangeListener implements PersistStateChangeListener {
		@Override
		public void onPersist(State<String, String> state, Message<String> message,
				Transition<String, String> transition, StateMachine<String, String> stateMachine) {
			if (message != null && message.getHeaders().containsKey("application")) {
				Application app = message.getHeaders().get("application", Application.class);
				app.setState(state.getId());
			}
			
		}
	}
}

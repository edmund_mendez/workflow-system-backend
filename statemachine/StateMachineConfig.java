package jm.gov.pica.statemachine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import jm.gov.pica.model.User;

public class StateMachineConfig {
	
	private static StateMachineConfig machine=null;
	public static StateMachineConfig startConfig(){	//for singleton pattern
		if(machine==null){
			return new StateMachineConfig();
		}
		return machine;
	}
	
	public enum OwnerTypes{
		AUTOMATIC,
		MAY_SELECT_IF_ROLE
	}
	
	public enum AutoOwnerTypes{
		FROM_FIELD,
		CURRENT_USER,
		FROM_PREVIOUS_ACTIVITY
	}
	
	private final HashMap<String,Config> configs=new HashMap<String,Config>();
	
	private String currentState="";
	private Config currentConfig=null ;
	
	
	private StateMachineConfig(){
		
	}
	
	public class Config{
		private String instructions="";
		private boolean requireApproval=false;
		private OwnerTypes ownerType;
		private String selectRoleList;
		private AutoOwnerTypes autoOwnerType;
		private Map<String,String> autoOwnerContext = new HashMap<String,String>();
		public String getInstructions() {
			return instructions;
		}
		public void setInstructions(String instructions) {
			this.instructions = instructions;
		}
		public boolean isRequireApproval() {
			return requireApproval;
		}
		public void setRequireApproval(boolean requireApproval) {
			this.requireApproval = requireApproval;
		}
		public OwnerTypes getOwnerType() {
			return ownerType;
		}
		public void setOwnerType(OwnerTypes ownerType) {
			this.ownerType = ownerType;
		}
		public String getSelectRoleList() {
			return selectRoleList;
		}
		public void setSelectRoleList(String selectRoleList) {
			this.selectRoleList = selectRoleList;
		}
		public AutoOwnerTypes getAutoOwnerType() {
			return autoOwnerType;
		}
		public void setAutoOwnerType(AutoOwnerTypes autoOwnerType) {
			this.autoOwnerType = autoOwnerType;
		}
		public Map<String, String> getAutoOwnerContext() {
			return autoOwnerContext;
		}
		public void setAutoOwnerContext(Map<String, String> autoOwnerContext) {
			this.autoOwnerContext = autoOwnerContext;
		}
	}
	
	public StateMachineConfig forState(String state){
		this.currentState = state;
		this.currentConfig = new Config();
		return this;
	}
	public StateMachineConfig instructions(String instructions){
		this.currentConfig.setInstructions(instructions);
		return this;
	}
	
	public StateMachineConfig requireApproval(){
		this.currentConfig.setRequireApproval(true);
		return this;
	}
	
	public StateMachineConfig officerClaimsWithRole(String roles){
		this.currentConfig.setOwnerType(OwnerTypes.MAY_SELECT_IF_ROLE);
		this.currentConfig.setSelectRoleList(roles);
		return this;
	}
	
	public StateMachineConfig officerAssignCurrent(){
		this.currentConfig.setOwnerType(OwnerTypes.AUTOMATIC);
		this.currentConfig.setAutoOwnerType(AutoOwnerTypes.CURRENT_USER);
		return this;
	}
	
	public StateMachineConfig officerAssignFromField(String fieldName){
		this.currentConfig.setOwnerType(OwnerTypes.AUTOMATIC);
		this.currentConfig.setAutoOwnerType(AutoOwnerTypes.FROM_FIELD);
		this.currentConfig.getAutoOwnerContext().put("fieldName", fieldName);
		return this;
	}
	
	public StateMachineConfig officerAssignFromPreviousActivity(String state){
		this.currentConfig.setOwnerType(OwnerTypes.AUTOMATIC);
		this.currentConfig.setAutoOwnerType(AutoOwnerTypes.FROM_PREVIOUS_ACTIVITY);
		this.currentConfig.getAutoOwnerContext().put("state", state);
		return this;
	}
	
	public StateMachineConfig and(){
		this.configs.put(this.currentState, this.currentConfig);
		this.currentConfig=null;
		this.currentState="";
		return this;
	}
	public StateMachineConfig Build(){
		return this;
	}
	
	public Config getConfigFor(String state){
		return this.configs.get(state);
	}
	

	//TODO: instead of using User implementation, consider a ActivityUser interface that can allow resuse!
	public StateMachineActivatable ConfigureAcitivity(String state, StateMachineActivatable newActivity,Collection<? extends StateMachineActivatable> prevActivities, User currentUser, HashMap<String,User> ownerFields){
		Config config = this.getConfigFor(state);
		newActivity.setSMInstructions(config.getInstructions());
		newActivity.setSMIsDecisionNode(config.isRequireApproval());
		if(config.getOwnerType().equals(OwnerTypes.MAY_SELECT_IF_ROLE)){
			newActivity.setSMRolesThatCanClaim(config.getSelectRoleList());//set to transient field that can be read by caller, then discarded
		}else if(config.getOwnerType().equals(OwnerTypes.AUTOMATIC)){
			//System.out.println("setting Automatic User!!");
			if(config.getAutoOwnerType().equals(AutoOwnerTypes.CURRENT_USER)){
				//System.out.println("setting to current user!!");
				newActivity.setSMAutomaticOwner(currentUser);
			}else if(config.getAutoOwnerType().equals(AutoOwnerTypes.FROM_FIELD)){
				newActivity.setSMAutomaticOwner(ownerFields.get(config.getAutoOwnerContext().get("fieldName")));
			}else if(config.getAutoOwnerType().equals(AutoOwnerTypes.FROM_PREVIOUS_ACTIVITY)){
				String preActivityName = config.getAutoOwnerContext().get("state");
				for(StateMachineActivatable act : prevActivities){
					if(act.getSMActivityState().equals(preActivityName)){		//need to check if previous activity is the *current* once, in case activity was done twice
						newActivity.setSMAutomaticOwner(act.getSMUserThatCompleted());
						break;
					}
				}
			}
		}else{
			//throw some exception - TODO
		}
		return newActivity;
	}
	
}

package jm.gov.pica.statemachine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.statemachine.state.State;

@Configuration
public class PersistHandlerConfig {
		@Autowired
		@Qualifier("stateMachineTarget")
		private StateMachine<String, String> stateMachine;
		
	    @Bean
	    public Persist persist() {
	        return new Persist(persistStateMachineHandler(),stateMachine);
	    }

	    @Bean
	    public PersistStateMachineHandler persistStateMachineHandler() {
	        return new PersistStateMachineHandler(stateMachine);
	    }
}

package jm.gov.pica;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import jm.gov.pica.model.ApplicationSource;
import jm.gov.pica.model.ApplicationType;
import jm.gov.pica.model.Country;
import jm.gov.pica.model.DescentApplication;
import jm.gov.pica.model.Privilege;
import jm.gov.pica.model.RenunciationApplication;
import jm.gov.pica.model.Role;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.CountryRepository;
import jm.gov.pica.repository.PrivilegeRepository;
import jm.gov.pica.repository.RoleRepository;
import jm.gov.pica.repository.SourceRepository;
import jm.gov.pica.repository.UserRepository;
import jm.gov.pica.services.ApplicationService;
import jm.gov.pica.services.AttachmentService;
import jm.gov.pica.statemachine.Persist;
import jm.gov.pica.statemachine.StateMachineConfig;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
	@Autowired
	private SourceRepository sourceRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private ApplicationService appService;
	
	@Autowired
	Persist persist;
	
	@Autowired
	private UserRepository userDao;
	@Autowired
    private RoleRepository roleDao;
	@Autowired
    private PrivilegeRepository privilegeDao;
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	//@Autowired
	//StateMachineConfig stMachineConfig;
	
	@Autowired
	AttachmentService attService;
	@Override
	  public void onApplicationEvent(final ApplicationReadyEvent event) {
	 
		//runScripts();
		//testStatemachine();
	    return;
	}
	private void createTempate(){
		attService.createMockTemplate(new File("c:/temp/descent_template.docx"), "Descent Basic Template", ApplicationType.DESCENT);
	}
	/*
	private void testStatemachine(){
		String ins = stMachineConfig.getConfigFor("ENTER_APP_DETAILS").getInstructions();
		System.out.println(ins);
		
		ins = stMachineConfig.getConfigFor("ATTACH_SUPP_DOCS").getInstructions();
		System.out.println(ins);
		
		ins = stMachineConfig.getConfigFor("ASSIGN_OFFICER").getInstructions();
		System.out.println(ins);
		
	}*/
	
	@SuppressWarnings("unused")
	private void runScripts(){
		createRolesPrivileges();
		createUsers();
		createSources();
		createCountries();
		createDescentapplication(userDao.findByUsername("emendez"));
		createRenunApplication(userDao.findByUsername("spassley"));
		createTempate();
	}
	private void createRenunApplication(User currentOwner) {
		// TODO Auto-generated method stub
		RenunciationApplication rApp = new RenunciationApplication(sourceRepository.findBySourceName("Mail-in"), "Jane", "Angie", "Brown", 'F', new Date(), "11 Alberta Way", "", "", countryRepository.findByName("Canada"), "","jbrown@gmail.com");
		rApp.setPlaceOfBirth("Montreal, Canada");
		rApp.setStatusCountry(countryRepository.findByName("Canada"));
		rApp.setCountryStatus("CITIZEN");
		rApp.setMarriageStatus("HAVE NOT");
		rApp.setRenunciateionDate(new Date());
		rApp.setApplicationType("RENUNCIATION");
		rApp.setCurrentOwner(currentOwner);
		appService.create(rApp,currentOwner);
	}

	private void createRolesPrivileges(){
		
	}
	
	private void createUsers(){
		Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
	    Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
	    List<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege);        
	    createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
	    createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));
	    createRoleIfNotFound("ROLE_PICA_CASE_OFFICER", adminPrivileges);
	    createRoleIfNotFound("ROLE_PICA_DESK_OFFICER", adminPrivileges);
	    createRoleIfNotFound("ROLE_PICA_MANAGER", adminPrivileges);
	    createRoleIfNotFound("ROLE_PICA_DIRECTOR", adminPrivileges);
	    createRoleIfNotFound("ROLE_PICA_CASHIER", adminPrivileges);
	    
	    
	    Role adminRole = roleDao.findByName("ROLE_ADMIN");
	    createUser("emendez","edmundm@gmail.com","Edmund","Mendez","password",Arrays.asList(adminRole));
	    createUser("spassley","edmundm@gmail.com","Suzanne","Passley","password",Arrays.asList(adminRole));
	    
	    createUser("caseofficer","cofficer@gmail.com","Case","Officer","password",Arrays.asList(roleDao.findByName("ROLE_PICA_CASE_OFFICER")));
	    createUser("deskofficer","dofficer@gmail.com","Desk","Officer","password",Arrays.asList(roleDao.findByName("ROLE_PICA_DESK_OFFICER")));
	    createUser("manager","manager@gmail.com","Citizenship","Manager","password",Arrays.asList(roleDao.findByName("ROLE_PICA_MANAGER")));
	    createUser("director","director@gmail.com","Citizenship","director","password",Arrays.asList(roleDao.findByName("ROLE_PICA_DIRECTOR")));
	    createUser("cashier","cashier@gmail.com","Citizenship","Cashier","password",Arrays.asList(roleDao.findByName("ROLE_PICA_CASHIER")));

	}
	private User createUser(String username, String email, String fname, String lname,String password, Collection<Role> roles){
		User user1 = new User();
		user1.setUsername(username);
		user1.setEmail(email);
		user1.setEnabled(true);
		user1.setFirstName(fname);
		user1.setLastName(lname);
		user1.setPassword(passwordEncoder.encode(password));
		user1.setRoles(roles);
		return userDao.save(user1);
	}
	private Privilege createPrivilegeIfNotFound(String name) {
	    Privilege privilege = privilegeDao.findByName(name);
	    if (privilege == null) {
	        privilege = new Privilege(name);
	        privilegeDao.save(privilege);
	    }
	    return privilege;
	}
	
	private Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
	    Role role = roleDao.findByName(name);
	    if (role == null) {
	        role = new Role(name);
	        role.setPrivileges(privileges);
	        roleDao.save(role);
	    }
	    return role;
	}
	
	private void createSources(){
		sourceRepository.save(new ApplicationSource("Walk-in"));
		sourceRepository.save(new ApplicationSource("Mail-in"));
		sourceRepository.save(new ApplicationSource("Online"));
		
	}
	
	private void createCountries(){
		
		countryRepository.save(new Country("Jamaica"));
		countryRepository.save(new Country("USA"));
		countryRepository.save(new Country("UK"));
		countryRepository.save(new Country("Canada"));
		countryRepository.save(new Country("Other"));
	}
	
	private void createDescentapplication(User currentOwner){
		//source, firstName, middleName, lastName, gender, dOB, address1, address2, address3, country, email
		
		DescentApplication dApp = new DescentApplication(sourceRepository.findBySourceName("Walk-in"), "Edmund", "", "Mendez", 'M', new Date(), "23 Oakwood Avenue", "Kingston 20", "", countryRepository.findByName("Jamaica"), "","Edmundm@gmail.com");
		
		dApp.setAppFirstName("Alexander");
		dApp.setAppMiddleNames("Edmund");
		dApp.setAppLastName("Mendez");
		dApp.setAddress1("7450 NW 84th Street");
		dApp.setAddress1("Plantation");
		dApp.setAddress1("Fl 33351");
		dApp.setAppDOB(new Date());
		dApp.setFatherFirstName("Edmund");
		dApp.setFatherLastName("Mendez");
		dApp.setFatherDOB(new Date());
		dApp.setFatherPOB("St. Mary, Jamaica");
		dApp.setApplicationType("DESCENT");
		dApp.setCurrentOwner(currentOwner);
		appService.create(dApp, currentOwner);
		

	}
}

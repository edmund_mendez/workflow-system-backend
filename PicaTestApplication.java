package jm.gov.pica;

import java.security.Principal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jm.gov.pica.utils.*;

@SpringBootApplication
@RestController

public class PicaTestApplication {
	@Autowired
	private Environment env;
	
	public static void main(String[] args) {
		SpringApplication.run(PicaTestApplication.class, args);
	}
	
	@RequestMapping("/app/user")
	  public Principal user(Principal user) {
	    return user;
	}
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
		//b.dateFormat(new SimpleDateFormat("dd/MM/yyyy"));
		b.deserializerByType(Date.class, new CustomDateDeserializer());
		b.serializerByType(Date.class, new CustomDateSerializer(env.getProperty("app-date-format")));
		return b;
	}
	
/*	@Bean
	public CommonsMultipartResolver multipartResolver(){
		CommonsMultipartResolver resolver= new CommonsMultipartResolver();
		resolver.setMaxUploadSize(20971520);
		return resolver;
	}*/
	
}

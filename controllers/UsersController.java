package jm.gov.pica.controllers;

import java.util.Collection;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jm.gov.pica.dto.UserLoadSummary;
import jm.gov.pica.services.ApplicationService;
import jm.gov.pica.services.UserService;

@RestController
public class UsersController {
	@Autowired
	private ApplicationService appService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/api/users",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<UserLoadSummary>> getUserLoad(@RequestParam String service){
		ResponseEntity<Collection<UserLoadSummary>> responseEntity=null;
		if(service==null){
			//get all users
		}else{
			if(service.equals("co_load_report")){
				responseEntity = new ResponseEntity<Collection<UserLoadSummary>>(userService.getUserLoadSummary(""),HttpStatus.OK);
			}
		}
		
		return responseEntity;
	}

}

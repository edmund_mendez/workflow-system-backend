package jm.gov.pica.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.LetterTemplate;
import jm.gov.pica.services.ApplicationService;
import jm.gov.pica.services.AttachmentService;
import jm.gov.pica.services.PublisherService;

@Controller
public class PublisherController {
	@Autowired
	private ApplicationService appService;
	
	@Autowired 
	private AttachmentService attService;
	
	@Autowired
	private PublisherService pubService;
	
	@RequestMapping(value="api/applications/{id}/certificate",method=RequestMethod.GET)
	@ResponseBody
    public void getFile(@PathVariable long id, @RequestParam long templateId, HttpServletResponse response) throws IOException{
		
		LetterTemplate template = attService.getTemplate(templateId);
		Application application=appService.findOne(id);
		String certFilename="certificate-"+application.getFirstName()+"_"+application.getLastName()+".docx";
		response.setContentType("application/force-download");
	    response.setHeader("Content-Disposition", "attachment; filename="+certFilename);
	        
		pubService.genCertificate(application, template, response.getOutputStream());
        response.flushBuffer();
       
    }
}

package jm.gov.pica.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationActivity;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.UserRepository;
import jm.gov.pica.security.SecurityUtils;
import jm.gov.pica.services.ApplicationService;
import jm.gov.pica.services.StateManagerService;

@RestController
public class ActivityController {
	@Autowired
	private ApplicationService appService;
	
	@Autowired
	private StateManagerService activityService;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value="/api/activities/application/{id}",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> createActivity(@PathVariable long id){
		Application application = appService.findOne(id);
		if(application!=null){
			activityService.completeActivity(application, "PROCESS");
		}
		//return new ResponseEntity<StateActivity>(application,HttpStatus.OK);
		return new ResponseEntity<Application>(application,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/activities/{id}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> completeActivity(
			@PathVariable long id,
			@RequestBody ApplicationActivity activityDTO, 
			HttpSession session){
		String event = "COMPLETE";
		if(activityDTO.isRequireApproval()){
			if(activityDTO.getApprovalResult()=='A')
				event="APPROVE";
			else
				event= "DECLINE";
		}
		System.out.println("event is "+event);
		Application updatedApp = activityService.completeActivity(activityDTO,event,getCurrentUser(session));
		return new ResponseEntity<Application>(updatedApp,HttpStatus.OK);
	}
	
	/*@RequestMapping(value="/api/activities/complete",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> updateActivity(@RequestBody Application application){
		activityService.completeActivity(application, "PROCESS");
		return new ResponseEntity<Application>(application,HttpStatus.OK);
	}*/
	
	private User getCurrentUser(HttpSession session){
		User user=null;
		if((user=(User)session.getAttribute("user"))==null){
			user = userRepository.findByUsername(SecurityUtils.getCurrentUserLogin());
			session.setAttribute("user",user);
		}
		return user;
	}
}

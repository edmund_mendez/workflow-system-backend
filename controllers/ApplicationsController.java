package jm.gov.pica.controllers;

import java.util.Collection;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import jm.gov.pica.dto.NewApplication;
import jm.gov.pica.dto.UserLoadSummary;
import jm.gov.pica.model.Application;
import jm.gov.pica.model.User;
import jm.gov.pica.repository.ApplicationOnlyProjection;
import jm.gov.pica.repository.UserRepository;
import jm.gov.pica.security.SecurityUtils;
import jm.gov.pica.services.ApplicationService;

@RestController
public class ApplicationsController {
	@Autowired
	private ApplicationService appService;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value="/api/applications",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<ApplicationOnlyProjection>> getApplications(@RequestParam String restrict, HttpSession session){
		Collection<ApplicationOnlyProjection> applications = appService.findRestricted(restrict, getCurrentUser(session));
		return new ResponseEntity<Collection<ApplicationOnlyProjection>>(applications,HttpStatus.OK);
	}
	
	//@JsonView(User.Views.Public.class)
	@RequestMapping(value="/api/applications/{id}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> getApplication(@PathVariable long id){
		Application application = appService.findOne(id);
		return new ResponseEntity<Application>(application,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/applications/{id}",method=RequestMethod.PATCH,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> patchApplication(@PathVariable long id, @RequestParam String field, @RequestBody String data){
		Application application = appService.findOne(id);
		return new ResponseEntity<Application>(appService.assignCaseOfficerByUsername(application,data),HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/applications/{id}",method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> updateApplication(@RequestBody Application application, @PathVariable long id){
		Application newApp = appService.update(application);
		return new ResponseEntity<Application>(newApp,HttpStatus.OK);
	}
	
	@RequestMapping(value="/api/applications/{id}",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> claimApplication(@PathVariable long id, @RequestParam String action, HttpSession session){
		Application application = appService.findOne(id);
		Application newApp = appService.claimApplication(application,getCurrentUser(session));
		if(application.getCurrentOwner()!=null){
			return new ResponseEntity<Application>(newApp,HttpStatus.OK);
		}else{
			return new ResponseEntity<Application>(newApp,HttpStatus.FORBIDDEN);
		}
	}
	
	@RequestMapping(value="/api/applications",method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Application> createApplication(@RequestBody NewApplication newAppDTO, HttpSession session){
		ResponseEntity<Application> responseEntity=null;
		responseEntity = new ResponseEntity<Application>(appService.createFromDTO(newAppDTO,getCurrentUser(session)),HttpStatus.OK);
		return responseEntity;
	}
	
	private User getCurrentUser(HttpSession session){
		User user=null;
		if((user=(User)session.getAttribute("user"))==null){
			user = userRepository.findByUsername(SecurityUtils.getCurrentUserLogin());
			session.setAttribute("user",user);
		}
		return user;
	}
}

package jm.gov.pica.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jm.gov.pica.model.Application;
import jm.gov.pica.model.ApplicationAttachment;
import jm.gov.pica.model.ApplicationType;
import jm.gov.pica.model.LetterTemplate;
import jm.gov.pica.services.ApplicationService;
import jm.gov.pica.services.AttachmentService;

@Controller
public class AttachmentsController {
	@Autowired 
	AttachmentService attService;
	
	@Autowired
	private ApplicationService appService;
	
	//@ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "api/applications/{id}/attachments",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ApplicationAttachment>> receiveUpload(@PathVariable long id, @RequestParam("file") MultipartFile file, @RequestParam("filetype") String filetype ){
		Application application=appService.findOne(id);
		ApplicationAttachment attachment = attService.createAppAttachment(application,file,filetype);
		application.getAttachments().add(attachment);
		appService.update(application);
        return new ResponseEntity<Collection<ApplicationAttachment>>(application.getAttachments(),HttpStatus.OK);
    }
    
    @RequestMapping(value = "api/templates",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LetterTemplate> receiveTempalte(@RequestParam("file") MultipartFile file, @RequestParam("templateName") String templateName , @RequestParam("applicationType") ApplicationType applicationType ){
		LetterTemplate template = attService.createTemplate(file, templateName, applicationType);
        return new ResponseEntity<LetterTemplate>(template,HttpStatus.OK);
    }
    
	@RequestMapping(value="api/applications/{appId}/attachments/{attId}",method=RequestMethod.GET)
	@ResponseBody
    public void getFile(@PathVariable long appId, @PathVariable int attId, HttpServletResponse response) throws IOException{
		ApplicationAttachment attachment = attService.getAppAttachmentFile(attId);

    	File file=null;
		try {
			file = attService.getAppFile(attachment);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		InputStream inputStream = new FileInputStream(file);
        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition", "attachment; filename="+attachment.getName()); 
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
        inputStream.close();
		//return new FileSystemResource(file);
    }
	

	    
    
}

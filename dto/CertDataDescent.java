package jm.gov.pica.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import jm.gov.pica.model.DescentApplication;

public class CertDataDescent {
	private String todaysdate;
	private String refnumber;
	private String fullname;
	private String gender;
	private String pob;
	private String dob;
	private String nationalityevidence;
	private String filiationevidence;
	private String parentfullname;
	private String parentgender;
	private String grandparentfullname;
	private String parentdob;
	private String parentpob;
	
	private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	public void transferData(DescentApplication app){
		this.todaysdate = df.format(new Date());
		this.refnumber = String.valueOf(app.getId());	
		this.fullname = app.getAppMiddleNames()!=null ? app.getAppFirstName() + " " +app.getAppMiddleNames()+" "+ app.getAppLastName() : app.getAppFirstName() + " " + app.getAppLastName(); 
		this.pob = app.getPlaceOfBirth();
		this.dob = df.format(app.getAppDOB());
		if(app.getFatherFirstName()!=null && app.getFatherFirstName().length()>0){	//using father
			this.nationalityevidence="Father's";
			this.filiationevidence="Paternal";
			this.parentfullname=app.getFatherFirstName()+" "+app.getFatherLastName();
			this.gender = app.getGender() == 'M' ? "male" : "female";
			this.parentgender="male";
			this.parentdob=df.format(app.getFatherDOB());
			this.parentpob=app.getFatherPOB();
		}else{	//using mother
			this.nationalityevidence="Mother's";
			this.filiationevidence="Maternal";
			this.parentfullname=app.getMotherFirstName()+" "+app.getMotherLastName();
			this.parentgender="female";
			this.parentdob=df.format(app.getMotherDOB());
			this.parentpob=app.getMotherPOB();
		}
		if(app.getGrandFatherFirstName()!=null && app.getGrandFatherFirstName().length()>0){
			this.grandparentfullname = app.getGrandFatherFirstName() + " " + app.getGrandFatherLastName();
		}else{
			this.grandparentfullname = app.getGrandMotherFirstName() + " " + app.getGrandMotherLastName();
		}
	}

	public String getTodaysdate() {
		return todaysdate;
	}

	public String getRefnumber() {
		return refnumber;
	}

	public String getFullname() {
		return fullname;
	}

	public String getGender() {
		return gender;
	}

	public String getPob() {
		return pob;
	}

	public String getDob() {
		return dob;
	}

	public String getNationalityevidence() {
		return nationalityevidence;
	}

	public String getFiliationevidence() {
		return filiationevidence;
	}

	public String getParentfullname() {
		return parentfullname;
	}

	public String getParentgender() {
		return parentgender;
	}

	public String getGrandparentfullname() {
		return grandparentfullname;
	}

	public String getParentdob() {
		return parentdob;
	}

	public String getParentpob() {
		return parentpob;
	}
	
}

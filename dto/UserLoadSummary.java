package jm.gov.pica.dto;

public class UserLoadSummary {
	private String username;
	private String firstName;
	private String lastName;
	private int applicationCount;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getApplicationCount() {
		return applicationCount;
	}
	public void setApplicationCount(int applicationCount) {
		this.applicationCount = applicationCount;
	}
	public String getFullName(){
		return this.firstName+" "+this.lastName;
	}
	public UserLoadSummary(String username, String firstName, String lastName, int applicationCount) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.applicationCount = applicationCount;
	}
	
}

package jm.gov.pica.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Privilege {
	public Privilege(String name) {
		this.name=name;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column
    private Long id;
	
	@Column
    private String name;
	
	@JsonIgnore
    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;
    
    public Privilege(){
    	
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public Long getId() {
		return id;
	}
    
    
}

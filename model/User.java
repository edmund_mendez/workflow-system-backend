package jm.gov.pica.model;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;
import com.mysql.fabric.xmlrpc.base.Array;

import javax.persistence.JoinColumn;
@Entity
@Table(name="Users")
public class User {
	 	@Id
	 	@GeneratedValue
	 	@Column
	 	int id;
	 	
	 	@NotNull
	    @Size(min = 1, max = 50)
	    @Column(length = 50, unique = true, nullable = false)
	 	private String username;
	 	
	 	@Column
	    private String firstName;
	 	@Column
	 	private String lastName;
	 	@Column
	 	private String email;
	 	
	 	@Column
	 	@JsonIgnore
	    @NotNull
	    @Size(min = 60, max = 60)
	 	private String password;
	 	
	 	@Column
	 	private boolean enabled;
	 	@Column
	 	private boolean tokenExpired;
	 	
	 	@JsonIgnore
	    @ManyToMany(fetch=FetchType.EAGER)	//TODO: NEED WORKAROUND
	    @JoinTable( 
	        name = "users_roles",
	        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), 
	        inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")) 
	    private Collection<Role> roles;
	    
	    //@JsonManagedReference
	    @JsonIgnore
	    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="currentOwner")
	    private Collection<Application> applications;
	    
	    //@JsonView(User.Views.Public.class)
		public int getId() {
			return id;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		//@JsonView(User.Views.Public.class)
		public String getUsername(){
			return username;
		}
		//@JsonView(User.Views.Public.class)
		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		//@JsonView(User.Views.Public.class)
		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
		//@JsonView(User.Views.Internal.class)
		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
		//@JsonView(User.Views.Internal.class)
		public boolean isTokenExpired() {
			return tokenExpired;
		}

		public void setTokenExpired(boolean tokenExpired) {
			this.tokenExpired = tokenExpired;
		}
		//@JsonView(User.Views.Internal.class)
		public Collection<Role> getRoles() {
			return roles;
		}

		public void setRoles(Collection<Role> roles) {
			this.roles = roles;
		}
		
		//@JsonView(User.Views.Internal.class)
		public Collection<Application> getApplications() {
			return applications;
		}

		public void setApplications(Collection<Application> applications) {
			this.applications = applications;
		}

		public User(){
			
		}
		
		@Override
		public String toString() {
			return firstName + " " + lastName;
		}
		
		public boolean hasRole(String[] targetRoles){
			Collection<Role> roles = this.getRoles();
			Collection<String> targetCollection = Arrays.asList(targetRoles);
			for(Role tmpRole : roles){
				if(targetCollection.contains(tmpRole.getName())){
					return true;
				}
			}
			return false;
		}
		
		public boolean hasRole(String targetRole){
			Collection<Role> roles = this.getRoles();
			for(Role tmpRole : roles){
				if(tmpRole.getName().equals(targetRole)){
					return true;
				}
			}
			return false;
		}
		
		
		public static final class Views {
		    // show only public data
		    public interface Public {}

		    // show public and internal data
		    public interface Internal extends Public {}
		}
}

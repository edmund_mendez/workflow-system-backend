package jm.gov.pica.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="id")
@JsonTypeName("Descent")
public class DescentApplication extends Application {
	public DescentApplication(ApplicationSource source, String firstName, String middleName, String lastName,
			char gender, Date birthDate, String address1, String address2, String address3, Country country, String phone, String email) {
		super(source, firstName, middleName, lastName, gender, birthDate, address1, address2, address3, country, phone, email);
	}
	public DescentApplication(){}
	private String appFirstName;
	private String appMiddleNames;
	private String appLastName;
	
	@Temporal(TemporalType.DATE)
	private Date appDOB;
	
	private String placeOfBirth;
	
	private String fatherFirstName;
	private String fatherMiddleName;
	private String fatherLastName;
	private String fatherPOB;
	@Temporal(TemporalType.DATE)
	private Date fatherDOB;
	
	private String motherFirstName;
	private String motherMiddleName;
	private String motherLastName;
	private String motherPOB;
	@Temporal(TemporalType.DATE)
	private Date motherDOB;
	
	private String grandFatherFirstName;
	private String grandFatherMiddleName;
	private String grandFatherLastName;
	private String grandFatherPOB;
	@Temporal(TemporalType.DATE)
	private Date grandFatherDOB;
	
	private String grandMotherFirstName;
	private String grandMotherMiddleName;
	private String grandMotherLastName;
	private String grandMotherPOB;
	@Temporal(TemporalType.DATE)
	private Date grandMotherDOB;
	public String getAppFirstName() {
		return appFirstName;
	}
	public void setAppFirstName(String appFirstName) {
		this.appFirstName = appFirstName;
	}
	public String getAppMiddleNames() {
		return appMiddleNames;
	}
	public void setAppMiddleNames(String appMiddleNames) {
		this.appMiddleNames = appMiddleNames;
	}
	public String getAppLastName() {
		return appLastName;
	}
	public void setAppLastName(String appLastName) {
		this.appLastName = appLastName;
	}
	public Date getAppDOB() {
		return appDOB;
	}
	public void setAppDOB(Date appDOB) {
		this.appDOB = appDOB;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}
	public String getFatherPOB() {
		return fatherPOB;
	}
	public void setFatherPOB(String fatherPOB) {
		this.fatherPOB = fatherPOB;
	}
	public Date getFatherDOB() {
		return fatherDOB;
	}
	public void setFatherDOB(Date fatherDOB) {
		this.fatherDOB = fatherDOB;
	}
	public String getMotherFirstName() {
		return motherFirstName;
	}
	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}
	public String getMotherMiddleName() {
		return motherMiddleName;
	}
	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}
	public String getMotherLastName() {
		return motherLastName;
	}
	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}
	public String getMotherPOB() {
		return motherPOB;
	}
	public void setMotherPOB(String motherPOB) {
		this.motherPOB = motherPOB;
	}
	public Date getMotherDOB() {
		return motherDOB;
	}
	public void setMotherDOB(Date motherDOB) {
		this.motherDOB = motherDOB;
	}
	public String getGrandFatherFirstName() {
		return grandFatherFirstName;
	}
	public void setGrandFatherFirstName(String grandFatherFirstName) {
		this.grandFatherFirstName = grandFatherFirstName;
	}
	public String getGrandFatherMiddleName() {
		return grandFatherMiddleName;
	}
	public void setGrandFatherMiddleName(String grandFatherMiddleName) {
		this.grandFatherMiddleName = grandFatherMiddleName;
	}
	public String getGrandFatherLastName() {
		return grandFatherLastName;
	}
	public void setGrandFatherLastName(String grandFatherLastName) {
		this.grandFatherLastName = grandFatherLastName;
	}
	public String getGrandFatherPOB() {
		return grandFatherPOB;
	}
	public void setGrandFatherPOB(String grandFatherPOB) {
		this.grandFatherPOB = grandFatherPOB;
	}
	public Date getGrandFatherDOB() {
		return grandFatherDOB;
	}
	public void setGrandFatherDOB(Date grandFatherDOB) {
		this.grandFatherDOB = grandFatherDOB;
	}
	public String getGrandMotherFirstName() {
		return grandMotherFirstName;
	}
	public void setGrandMotherFirstName(String grandMotherFirstName) {
		this.grandMotherFirstName = grandMotherFirstName;
	}
	public String getGrandMotherMiddleName() {
		return grandMotherMiddleName;
	}
	public void setGrandMotherMiddleName(String grandMotherMiddleName) {
		this.grandMotherMiddleName = grandMotherMiddleName;
	}
	public String getGrandMotherLastName() {
		return grandMotherLastName;
	}
	public void setGrandMotherLastName(String grandMotherLastName) {
		this.grandMotherLastName = grandMotherLastName;
	}
	public String getGrandMotherPOB() {
		return grandMotherPOB;
	}
	public void setGrandMotherPOB(String grandMotherPOB) {
		this.grandMotherPOB = grandMotherPOB;
	}
	public Date getGrandMotherDOB() {
		return grandMotherDOB;
	}
	public void setGrandMotherDOB(Date grandMotherDOB) {
		this.grandMotherDOB = grandMotherDOB;
	}
	
	
}

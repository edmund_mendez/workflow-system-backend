package jm.gov.pica.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class LetterTemplate {
	@Id
	@GeneratedValue
	private long id;
	
	private String templateName;
	
	@Enumerated(EnumType.STRING)
	private ApplicationType applicationType;
	private String fileName;
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public ApplicationType getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(ApplicationType applicationType) {
		this.applicationType = applicationType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public long getId() {
		return id;
	}	
}

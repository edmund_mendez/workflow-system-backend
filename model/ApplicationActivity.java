package jm.gov.pica.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jm.gov.pica.statemachine.StateMachineActivatable;

@Entity
@Table(name="Activities")
public class ApplicationActivity implements StateMachineActivatable{
	@Id
	@GeneratedValue
	private long id;
	private String SmState;
	private boolean isCompleted;
	private boolean isArchived;
	private String comments;
	private boolean requireApproval;
	private char approvalResult;
	
	private String instructions;
	
	@JsonIgnore
	@Transient
	private String claimGroups;	//used for data transfer to application 
	
	@JsonIgnore
	@Transient
	private User automaticOwner; //used for data transfer to application
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateStarted;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCompleted;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="application_id")
	private Application application;
	
	@JsonIgnoreProperties({"email", "password","enabled","tokenExpired","roles","applications"})
	@ManyToOne
	@JoinColumn(name="user_id")
	private User completedBy;

		
	public String getSmState() {
		return SmState;
	}

	public void setSmState(String smState) {
		SmState = smState;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public boolean isArchived() {
		return isArchived;
	}

	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}

	public Date getDateStarted() {
		return dateStarted;
	}

	public void setDateStarted(Date dateStarted) {
		this.dateStarted = dateStarted;
	}

	public Date getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public User getCompletedBy() {
		return completedBy;
	}

	public void setCompletedBy(User completedBy) {
		this.completedBy = completedBy;
	}

	public long getId() {
		return id;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isRequireApproval() {
		return requireApproval;
	}

	public void setRequireApproval(boolean requireApproval) {
		this.requireApproval = requireApproval;
	}

	public char getApprovalResult() {
		return approvalResult;
	}

	public void setApprovalResult(char approvalResult) {
		this.approvalResult = approvalResult;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getClaimGroups() {
		return claimGroups;
	}

	public void setClaimGroups(String claimGroups) {
		this.claimGroups = claimGroups;
	}

	public User getAutomaticOwner() {
		return automaticOwner;
	}

	public void setAutomaticOwner(User automaticOwner) {
		this.automaticOwner = automaticOwner;
	}
	
	/***************** StateMachineActivatable interface implementations *****************/
	@Override
	public void setSMInstructions(String instructions) {
		this.setInstructions(instructions);
	}

	@Override
	public void setSMAutomaticOwner(User owner) {
		this.setAutomaticOwner(owner);
	}

	@Override
	public void setSMRolesThatCanClaim(String roles) {
		this.setClaimGroups(roles);
	}

	@Override
	public void setSMIsDecisionNode(boolean isDecision) {
		this.setRequireApproval(isDecision);
	}
	@JsonIgnore
	@Override
	public String getSMActivityState() {
		return this.getSmState();
	}
	@JsonIgnore
	@Override
	public User getSMUserThatCompleted() {
		return this.getCompletedBy();
	}
}

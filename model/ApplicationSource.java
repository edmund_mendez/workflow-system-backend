package jm.gov.pica.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sources")
public class ApplicationSource {
	@Id
	@GeneratedValue
	private int id;
	private String sourceName;
	
	public ApplicationSource(){
		
	}
	public ApplicationSource(String sourceName) {
		super();
		this.sourceName = sourceName;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

}

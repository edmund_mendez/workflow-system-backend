package jm.gov.pica.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Table(name="Applications")
@Inheritance( strategy = InheritanceType.JOINED )
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = DescentApplication.class, name = "Descent"),
    @JsonSubTypes.Type(value = RenunciationApplication.class, name = "Renunciation")
})
//@NamedQuery(name="Application.findUserWorkload",query="SELECT u, COUNT(a) FROM Users u LEFT OUTER JOIN Application a")
public class Application implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8299754272422045059L;

	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	private ApplicationSource source;
	private String firstName;
	private String middleName;
	private String lastName;
	private char gender;
	
	//@JsonSerialize(using = CustomDateSerializer.class)
    //@JsonDeserialize(using = CustomDateDeserializer.class)
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	private String address1;
	private String address2;
	private String address3;
	@OneToOne
	private Country country;
	private String phone;
	private String email;
	private String refrenceNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate=new Date();
	private String applicationType;
	private String state;
	
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="application")
	private Collection<ApplicationActivity> activities = new HashSet<>();
	
	@ManyToOne
    @JoinColumn(name="owner_id")
	@JsonIgnoreProperties({"email", "password","enabled","tokenExpired","roles","applications"})
	private User currentOwner;
	
	//@JsonIgnore
	private String claimRoles;	//comma-delimited list of ROLES user must have to claim
	
	@ManyToOne
	@JsonIgnoreProperties({"email", "password","enabled","tokenExpired","roles","applications"})
	private User caseOfficer;
	
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="application")
	private Collection<ApplicationAttachment> attachments= new HashSet<>();

	
	public Application(ApplicationSource source, String firstName, String middleName, String lastName, char gender,
			Date birthDate, String address1, String address2, String address3, Country country,String phone, String email) {
		super();
		this.source = source;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthDate = birthDate;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.country = country;
		this.phone=phone;
		this.email = email;
	}
	public Application(){}
	//@JsonView(User.Views.Public.class)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	//@JsonView(User.Views.Public.class)
	public ApplicationSource getSource() {
		return source;
	}
	public void setSource(ApplicationSource source) {
		this.source = source;
	}
	//@JsonView(User.Views.Public.class)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	//@JsonView(User.Views.Public.class)
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	//@JsonView(User.Views.Public.class)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	//@JsonView(User.Views.Public.class)
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	//@JsonView(User.Views.Public.class)
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRefrenceNumber() {
		return refrenceNumber;
	}
	public void setRefrenceNumber(String refrenceNumber) {
		this.refrenceNumber = refrenceNumber;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Collection<ApplicationActivity> getActivities() {
		return activities;
	}
	public void setActivities(Collection<ApplicationActivity> activities) {
		this.activities = activities;
	}
	//@JsonView(User.Views.Public.class)
	public User getCurrentOwner() {
		return currentOwner;
	}
	public void setCurrentOwner(User currentOwner) {
		this.currentOwner = currentOwner;
	}
/*	public String getOwnerTest(){
		User owner =  this.getCurrentOwner();
		if(owner!=null)
			return owner.getFirstName();
		return "nothing to see";
		//return this.currentOwner.getUsername();
	}*/
	public String getClaimRoles() {
		return claimRoles;
	}
	public void setClaimRoles(String claimRoles) {
		this.claimRoles = claimRoles;
	}
	public User getCaseOfficer() {
		return caseOfficer;
	}
	public void setCaseOfficer(User caseOfficer) {
		this.caseOfficer = caseOfficer;
	}
	public Collection<ApplicationAttachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(Collection<ApplicationAttachment> attachments) {
		this.attachments = attachments;
	}
	
	
	
}

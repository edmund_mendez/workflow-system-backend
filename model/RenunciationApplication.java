package jm.gov.pica.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="id")
@JsonTypeName("Renunciation")
public class RenunciationApplication extends Application {
	public RenunciationApplication(ApplicationSource source, String firstName, String middleName, String lastName,
			char gender, Date birthDate, String address1, String address2, String address3, Country country, String phone, String email) {
		super(source, firstName, middleName, lastName, gender, birthDate, address1, address2, address3, country, phone, email);
	}
	public RenunciationApplication(){}
	
	//private String POB;
	private String placeOfBirth;
	private String marriageStatus;
	private String countryStatus;
	@OneToOne
	private Country statusCountry;
	
	@Temporal(TemporalType.DATE)
	private Date renunciateionDate;	//REFACTOR TO ADDRESS SPELLING ERROR******
	
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getMarriageStatus() {
		return marriageStatus;
	}
	public void setMarriageStatus(String marriageStatus) {
		this.marriageStatus = marriageStatus;
	}
	public String getCountryStatus() {
		return countryStatus;
	}
	public void setCountryStatus(String countryStatus) {
		this.countryStatus = countryStatus;
	}
	
	public Country getStatusCountry() {
		return statusCountry;
	}
	public void setStatusCountry(Country statusCountry) {
		this.statusCountry = statusCountry;
	}
	public Date getRenunciateionDate() {
		return renunciateionDate;
	}
	public void setRenunciateionDate(Date renunciateionDate) {
		this.renunciateionDate = renunciateionDate;
	}
	
	
}
